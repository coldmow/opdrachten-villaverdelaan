<?php
require('login_checker_code.php');
?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Opdracht: Inloggen</title>
</head>
<body>

<?php if ( isset($_SESSION['id']['gebruiker-id']) && isset($_SESSION['id']['ww-id']) ): ?>

    U bent INGELOGD :D
    <form action="" method="post">
        <input type="submit" name="sub_log_uit" value="Uitloggen?">
    </form>

<?php else: ?>

    <form action="" method="post">
        <label for="gnaam"> Gebruikersnaam: </label>    <input type="text" name="gnaam1">   <br>
        <label for="ww">    Wachtwoord:     </label>    <input type="password" name="ww1">  <br>
        <input type="submit" value="Inloggen" name="sub_log_in">
    </form>

<?php endif; ?>



</body>
</html>