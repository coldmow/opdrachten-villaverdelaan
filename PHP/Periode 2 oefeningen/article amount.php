<?php

//file model:
abstract class Articles{

	protected static $articlecount = 0;
	
	protected $name = '';
	protected $kopTekst = '';
	protected $articleTekst = '';

	public function showHTML(){
		return '<h1>'.$this->kopTekst.'</h1>'.'<br>'.$this->articleTekst;
	}

	public static function getArticleCount(){
		return self::$articlecount;
	}
	public static function increaseArticleCount(){
		self::$articlecount ++;
	}
}

class Buitenland extends Articles{

	public function __construct ($kopTekst, $articleTekst){
		self::$articlecount++;
		$this->kopTekst = $kopTekst;
		$this->articleTekst = $articleTekst;
	}


}

class Economie extends Articles{

	public function __construct ($kopTekst, $articleTekst){
		self::$articlecount ++;
		$this->kopTekst = $kopTekst;
		$this->articleTekst = $articleTekst;
	}

	public function setNaam($naam){
		$this->name = $naam;
	}

	public function getNaam(){
		return $this->naam;
	}
}

//file controller:

$article = new Economie(
"
	De hoge temperaturen en de regen die lange tijd uitbleef, waren ongunstige weersomstandigheden voor de citrusvrucht."
	,
	"<p>Het te warme weer van Marokko en te lang uitblijven van regen zal naar verwachting leiden tot een sterke daling van de volumes van citrusvruchten geproduceerd in het Koninkrijk in vergelijking met voorgaande jaren, meldt Fresh Fruit portal. Hierdoor zal er dus ook een daling zijn in de export.</p>
	
	<p>Er wordt een daling verwacht van 15 tot 20%, van 2,3 miljoen ton vorig jaar tot ongeveer 2 miljoen ton dit jaar, volgens een verklaring van Agri Morocco. Ongeveer vier maanden lang was de temperatuur in Marokko ongewoon hoog, waardoor droogte ontstond en de oogst kleiner werd.</p>

	<p>De oostelijke regio en de regio Marrakech-Safi zouden het meest zijn getroffen door ongunstige weersomstandigheden. Het citrusseizoen in Marokko loopt meestal van oktober tot halverwege het jaar.</p>

	<p>Volgens een rapport van het US Department of Agriculture (USDA) dat in december 2016 werd vrijgegeven, steeg de citrusproductie in Marokko in het vorige seizoen met 15%, dus de daling dit seizoen zou vergelijkbaar moeten zijn met de oogst van 2015-2016.</p>
" );

//file view:

echo $article->showHTML();

//let op: bij oproepen van een static ALTIJD "::$" gebruiken, het is dus niet hetzelfde als wat bij properties "->" is.
echo $article->getArticleCount();

echo '<br>' . Articles::getArticleCount();  // !! werkt niet !! 

$article->increaseArticleCount();
$article->increaseArticleCount();
$article->increaseArticleCount();
echo '<br>' . $article->getArticleCount();





/* 
Notities: 

'Solid principles':
systeem(programma)	dat open staat voor uitbereiding, maar gesloten voor modificatie, door:

programma heeft classes waar niemand aan mag zitten, maar accepteert wel nieuwe classes (van bijv. werknemers).

*/