<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="initial-scale=1.0">
	<meta http-equiv="X-UA-Compatible" content="ie=edge">
	<title>Document</title>
	
	<style>
		/* CSS styling */
		body { 
			text-align:center;
			font-family: arial;
		}
		.cups { 
			margin:20px auto 70px;
			width:520px;
			height:183px;
			border-bottom:3px solid #000;
		}
		.cup { 
			width:132px;
			height:113px;
			background-image:url(cup.png);
			float:left;
			margin:0 20px;

		}
		.ball { 
			position:absolute;
			margin-top:72px;
			margin-left:44px;
			z-index:-1;
			width:43px;
			height:41px;
			background:url(ball.png);
		}
		.putdown { 
			margin-top:70px;
		}
		.putdown .ball { 
			margin-top:72px;
		}
		.liftup { 
			margin-top:0px;
		}
		.liftup .ball { 
			margin-top:142px;
		}
		.player { 
			margin:0 auto;
			width:92px;
			height:243px;
			background:transparent url(player.png) no-repeat 0 60px;
		}
		.clear { 
			clear:both;
		}
		.red { 
			background-color:red;
		}
		.yellow { 
			background-color:yellow;
		}
		.blue {
			background-color:blue;
		}
	</style>
	
</head>
<body>
	
<!-- <div class="cups">
	
	<div class="cup yellow liftup">
		<div class="ball red"></div>
	</div>
	
	<div class="cup yellow putdown"></div>
	
	<div class="cup yellow"></div>
	
	<div class="clear"></div>
	
	</div>
		<div class="player">
		<strong>*name*: *amount*</strong>
	</div> -->
	
	<div class="cups">
		
			<?php 
				echo $cupArray[0]->show().
					
				'</div>'.
				$cupArray[1]->show(). 
					
				'</div>'.
				$cupArray[2]->show(). 
					
				'</div>';
			?>

			<div class="clear"></div>
		</div>
		
		<?php 
		   echo $player->show();
		?>

</body>
</html>