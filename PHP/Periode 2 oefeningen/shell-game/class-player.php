<?php

class Player{
	public $name = NULL;
	public $amount = NULL;

	public function setName($name){
		$this->name = $name;
	}

	public function setAmount($amount){
		$this->amount = $amount;
	}

	public function show(){
		$HTMLstring = 
		'<div class="player">
			<strong>'.$this->name.': '.$this->amount.'</strong>
		</div>';
		
		return $HTMLstring;
	}
}