<?php

/**
 * Klasse die de bekertjes representeert.
 */
class Cup{
	public $color = NULL;
	public $type = NULL;
	public $positionUp = NULL;
	public $ball = NULL;



	public function setColor($color){
		$this->color = $color;
	}
	public function setType($type){
		$this->type = $type;
	}


	
	public function liftUp(){
		$this->positionUp = true;
	}
	public function putDown(){
		$this->positionUp = false;

	}



	public function show(){

		$htmlCup = '<div class="cup '.
			$this->color;
		$htmlClassUp = ' liftup ';
		$htmlClassDown = ' putdown ';
		$htmlEnd = ' "> ';

		if($this->positionUp)
		{
			$returnedHtml = $htmlCup.$htmlClassUp.$htmlEnd;

		}elseif (!$this->positionUp) 
		{
			$returnedHtml = $htmlCup.$htmlEnd;
		}
		if($this->ball instanceof Ball){
			// $ball = $this->ball;
			// $ball->show();
			$returnedHtml .= $this->ball->show();
		}

		return $returnedHtml;
	}
}
