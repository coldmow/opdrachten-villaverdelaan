<?php

class Ball{
	public $color = NULL;

	public function setBallColor($color){
		$this->color = $color;
	}
	public function show(){
		$HTMLstring = '
		<div class="ball '.$this->color.'"></div>';
		return $HTMLstring; 
	}
}