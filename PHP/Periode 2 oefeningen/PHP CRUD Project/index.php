<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta http-equiv="X-UA-Compatible" content="ie=edge">
	<title>Document</title>
</head>
<body>


	<?php

	if(isset($_GET['controller'])){
		
		$controller = filter_input(INPUT_GET, 'controller',FILTER_SANITIZE_URL);
		$action = filter_input(INPUT_GET, 'action', FILTER_SANITIZE_URL);
		

		$className = ucfirst($controller) . 'Controller';
		
		$controllerFile = 'controllers/' . $className . '.php';
		
		if (file_exists($controllerFile)) {
			require_once $controllerFile;
		} else {
			throw new Exception('Controller file "'.$controllerFile.'" does not exist'); 
		}
		
		
		
		/*
		Wat interessant is is dat je een variabele kunt gebruiken als functienaam
		om zo een flexibele object-instantie-initialisatie regel te maken:  */
		if (class_exists($className)) {
			$controllerObject = new $className();
		} else {
			throw new Exception('Class  "'.$className.'" does not exist'); 
		}
		
		
		/* Het zelfde geldt voor de methode-instantie-initialisatie: */
		if (method_exists($controllerObject, $action)) {
			$controllerObject->{$action}();
		} else {
			throw new Exception('Action  "'.$action.'" does not exist'); 
		}
		
	}else{
		echo '
			<a href="index.php?controller=page&action=home">Home</a> <br><br>
			<a href="index.php?controller=page&action=about">About</a>
		';
	}

	?>

</body>
</html>