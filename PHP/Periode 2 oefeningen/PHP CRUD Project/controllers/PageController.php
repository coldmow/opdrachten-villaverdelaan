<?php

class PageController{
	
	public function home(){
		include "views/theme/header.php";
		include "views/pages/home.php";
		include "views/theme/footer.php";
	} 

	public function about(){
		include "views/theme/header.php";
		include "views/pages/about.php";
		include "views/theme/footer.php";
	}
	
}