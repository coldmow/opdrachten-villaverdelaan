<?php 

/**
* 
*/
class Person{

	public $name;

	public $age;

	public function __construct($name, $age){
		$this->name = $name;
		$this->age = $age;
	}
	
	public function showInfo(){

		return 'dit is '.$this->name.', hij/zij is '.$this->age.' jaar.';
	} 	
}

// $persoon = new Person('Tim', 20);

// $sharon = new Person('Sharon', 94);

// echo "\n";
// echo "\n";

// echo $persoon->showInfo();

/*als javascript een taal is dat gemaakt is voor de browser dan vind ik het verdacht dat de complete interactie met de browser via een object (window) moet,
dit laat juist zien dat de browser een extensie ervan is*/


class Car{

	public $brand		= null;
	public $model		= null;
	private $fuel		= null;
	private $color		= null;
	public $eigenaar	= null;

	public function __construct( $brand ,$model ,$fuel ,$color ,$eigenaar){
		$this->brand = $brand;
		$this->model = $model;
		$this->fuel = $fuel;
		$this->color = $color;
		$this->eigenaar = $eigenaar;
	}

	public function showCarInfo(){
		return  
		$this->brand.
		$this->model.
		$this->fuel.
		$this->color.
		$this->eigenaar;
	}
	public function isFuelEfficiënt(){
		switch ($this->fuel) {
			case 'benzine':
				return "benzine";
				break;
			case 'diesel':
				return "diesel";
				break;
			case 'flowerpower':
				return 'true';
				break;
			case 'skeere benzine uit Algerije':
				return 'DISASTER';
				break;
			case 'sla-olie':
				return 'crimineel';
				break;
			case 'electrisch':
				return 'goeie man';
				break;
			
			default:
				return 'ERROR';
				break;
		}
	}
	public function setFuel($fuel){
		$this->fuel = $fuel;
	}
	public function getFuel(){
		return $this->fuel;
	}
}

$henkie = new Car('Mercedes', 'C63 AMG', 'skeere benzine uit Algerije', 'matte-zwart', 'Henk');

echo $henkie->isFuelEfficiënt();

$henkie->setFuel('benzine');

$spijtigeHenkie = $henkie->getFuel();
echo $spijtigeHenkie

?>