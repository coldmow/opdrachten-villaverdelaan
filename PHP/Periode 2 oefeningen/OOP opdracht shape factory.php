<?php
$circle = ShapeFactory::create('circle', 3);
$circleArea = $circle->getArea();

echo $circleArea;  // 3 * 3 * pi

$square = ShapeFactory::create('square', 3);
$squareArea = $square->getArea();

echo $squareArea;  // 3 * 3