<?php

$values = array(10,6,2);

// Functies: Sum , Min , Multi , Div .
$sum1 = new Div($values);
echo $sum1->calculeer();

class Rekenen{
	
	protected $getallen = [];
	
	public function __construct($nummer){
		// $this->getal1 = $num1;
		// $this->getal2 = $num2;
		
		$this->setGetallen($nummer);
	}

	public function getGetallen(){
		return $this->getallen;
	}
	
	public function setGetallen($nummer){
		$this->getallen = $nummer;
	}
}



class Sum extends Rekenen {
	
	public function calculeer(){
		$uitkomst = 0;

		foreach($this->getallen as $currentValue){
			$uitkomst += $currentValue;
		}
		return $uitkomst;
	}
}



class Min extends Rekenen {
	
	public function calculeer(){
		$uitkomst = $this->getallen[0];
		
		for ($i=1; $i < count($this->getallen); $i++) { 
			$uitkomst -= $this->getallen[$i];
		}
		return $uitkomst;
	}
}



class Multi extends Rekenen {
	
	public function calculeer(){
		$uitkomst = $this->getallen[0];
		
		for ($i=1; $i < count($this->getallen); $i++) { 
			$uitkomst *= $this->getallen[$i];
		}
		return $uitkomst;
	}
}



class Div extends Rekenen {
	
	public function calculeer(){
		$uitkomst = $this->getallen[0];
		
		for ($i=1; $i < count($this->getallen); $i++) { 
			$uitkomst /= $this->getallen[$i];
		}
		return $uitkomst;
	}
}

