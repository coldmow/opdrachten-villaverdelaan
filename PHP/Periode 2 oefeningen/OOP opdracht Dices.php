<?php

$diceNbrs = array(1,2,3,4,5,6);
$rollit = new Dice($diceNbrs);

//Genereert een nieuw willekeurig getal en geeft hem (als extra) terug:
echo $rollit->roll();

//Geeft het gegenereede getal terug:
echo $rollit->getNumber();

//Geeft de met het willekeurig getal corresponderende bestandsnaam terug in een html element zodat de juiste afbeelding verschijnt:
echo $rollit->displayDice();





class Dice {

	private $randomNum = NULL; // Willekeurig nummer.
	private $diceNumbz = NULL; // Array met alle dobbelsteen nummers.

	public function __construct($dicetype){
		$this->diceNumbz = $dicetype;
		$this->randomNum = $this->diceNumbz[array_rand($this->diceNumbz)];
	}

	public function roll(){
		$this->randomNum = $this->diceNumbz[array_rand($this->diceNumbz)];
		return $this->randomNum;
	}

	public function getNumber(){
		return $this->randomNum;
	}

	// Hoe kun je het beste deze herhalende functies generiek maken??:
	public function displayDiceNormal(){
		$htmlstring[0] = '<img src="media/icons/delapouite/dice/png/000000/
		transparent/dice-six-faces-';
		$htmlstring[1] = '.png" width="300px"/>';

		switch ($this->randomNum) {
			case '1':
				return $htmlstring[0]."one".$htmlstring[1];
			break;

			case '2':
				return $htmlstring[0]."two".$htmlstring[1];
			break;

			case '3':
				return $htmlstring[0]."three".$htmlstring[1];
			break;

			case '4':
				return $htmlstring[0]."four".$htmlstring[1];
			break;

			case '5':
				return $htmlstring[0]."five".$htmlstring[1];
			break;

			case '6':
				return $htmlstring[0]."six".$htmlstring[1];
			break;
		}
	}
	//Wanneer moet er een sub klasse gemaakt worden??
}
