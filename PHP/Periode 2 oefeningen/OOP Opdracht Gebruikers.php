<?php

class User{

	protected $username = "";
	protected $password = "";

	// instantieert een nieuwe gebruiker:
	public function __construct($usern, $passw){
		$this->username = $usern;
		$this->password = hash("sha256", $passw, false);
	}

	// Geeft de gebruikersnaam en wachtwoord(hash) terug.
	public function getUserData(){
		return "Username: ".$this->username."\n".
		"Password: ".$this->password."\n";
	}

	// Biedt de mogelijkheid het wachtwoord te veranderen:
	public function changePass($newPass){
		if(
			$this->password = hash("sha256", $newPass, false)
		){
			return true;
		}else{
			return false;
		}
	}

	// Checkt of het wachtwoord correct is:
	public function verifyPass($pass2verify){
		if(
			hash("sha256", $pass2verify, false) == $this->password
		){
			return true ."Password correct!\n";
		}else{
			return false ."Wrong password!\n";
		}
	}

	public function doSuperviserStuff(){	}
	public function doNormalUserStuff(){	}
	
	// Wat bedoelt u met: 
	//"Zou je stap 7 en 8 ook op een andere manier kunnen implementeren? (generieker)" ?
}

$user1 = new User("tomas", "legend32");
$user2 = new User("anna", "whatevah96");

echo $user2->getUserData();
echo $user2->verifyPass("apenkool");
echo $user2->verifyPass("whatevah96");

?>