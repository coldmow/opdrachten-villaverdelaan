multiD = [
	[9,21,14,1,2,3,4,5,6,7,8,9,10],
	[11,5,27,1,2,3,4,5,6,7,8,9,10],
	[29,17,6,1,2,3,4,5,6,7,8,9,10]
];


function sumRow(array,rij){
	var uitkomst = 0;
	
	array[rij].forEach(function(value, index, arr){
		uitkomst = uitkomst + value;
	});

	return uitkomst;
}
function sumMulti(mdArray){
	var uitkomst2 = 0;

	mdArray.forEach(function(value, index, arr){
		
		value.forEach(function(val1, index1, arr1) {
			uitkomst2 = uitkomst2 + val1;
		});
	});
	return uitkomst2;
}


console.log(sumRow(multiD,1));

console.log(sumMulti(multiD));

